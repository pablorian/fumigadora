
$( document ).ready(function() {

  $('.bt_delete').click(function deleteClick(e) {
      var delete_url = $(this).attr('data-url');
      bootbox.confirm("¿Está seguro?", function (result) {
          if (result) {
            $.ajax({
              url: delete_url,
              type: 'DELETE',
              success: function(result) {
                  location.reload(false);
              }
            });
          }
      });

  });


  var table = $('#main_table');

  // begin first table
  table.dataTable({

      // Or you can use remote translation file
      "language": {
         url: '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Spanish.json'
      },

      // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
      // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js).
      // So when dropdowns used the scrollable div should be removed.
      //"dom": "<'row'<'col-md-6 col-sm-12'l><'col-md-6 col-sm-12'f>r>t<'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>",

      "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.

      "lengthMenu": [
          [5, 15, 20, -1],
          [5, 15, 20, "All"] // change per page values here
      ],
      // set the initial value
      "pageLength": 10,
      "pagingType": "bootstrap_full_number",
      "columnDefs": [
          {  // set default column settings
              'orderable': false,
              'targets': [0]
          },
          {
              "searchable": false,
              "targets": [0]
          },
          {
              "className": "dt-right",
              //"targets": [2]
          }
      ],
      "order": [
          [1, "asc"]
      ] // set first column as a default sort by asc
  });
}); //End document ready
