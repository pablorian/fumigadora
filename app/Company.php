<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
	use Notifiable;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'city', 'address', 'phone',
    ];

    public function plants()
    {
       $this->hasMany('App\Plant');
    }

    public function users()
	{
		return $this->belongsToMany('App\User');
	} 
}
