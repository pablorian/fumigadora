<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Technician;
use View;
use Session;


class TechnicianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $Technicians = Technician::all();

        // load the view and pass the users
        return View::make('technicians.index')
            ->with('technicians', $Technicians);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:companys',
            'email' => 'required|email|max:255|unique:companys',
            'address' => 'required|max:255',
            'ciudad' => 'required|max:255',
            'phone' => 'required|numeric|max:20',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'ciudad' => $data['ciudad'],
            'phone' => $data['phone'],
        ]);*/

        return view('technicians.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/

        $technician = new Technician;
        $technician->name = $request->name;
        $technician->DNI = $request->DNI;
        $technician->Title = $request->Title;

        $technician->save();
        Session::flash('message', 'Tecnico guardado con éxito!');
        return redirect()->route('technicians.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $technician = Technician::find($id);
        return view('technicians.edit')->with('technician', $technician);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/
        $technician = Technician::find($id);
        $technician->name = $request->name;
        $technician->DNI = $request->DNI;
        $technician->Title = $request->Title;

      $technician->save();
      Session::flash('message', 'Tecnico modificado con éxito!');
      return redirect()->route('technicians.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $technician = Technician::find($id);
        $technician->delete();
        return response()->json([
          'status' => 'ok'
      ]);
    }
}
