<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Plant;
use View;
use Session;


class PlantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $plants = Plant::all();

        // load the view and pass the users
        return View::make('plants.index')
            ->with('plants', $plants);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:companys',
            'email' => 'required|email|max:255|unique:companys',
            'address' => 'required|max:255',
            'ciudad' => 'required|max:255',
            'phone' => 'required|numeric|max:20',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'ciudad' => $data['ciudad'],
            'phone' => $data['phone'],
        ]);*/

        return view('plants.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/

        $plant = new Plant;
        $plant->name = $request->name;
        $plant->summary = $request->summary;
        $plant->observations = $request->observations;
        $plant->image_plane = "imagen";
        $plant->company_id = $request->company_id;

        $plant->save();
        Session::flash('message', 'Planta guardada con éxito!');
        return redirect()->route('plants.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $plant = Plant::find($id);
        return view('plants.edit')->with('plant', $plant);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/
        $plant = Plant::find($id);
        $plant->name = $request->name;
        $plant->summary = $request->summary;
        $plant->observations = $request->observations;
        //$plant->image_plane = $request->image_plane;
        $plant->company_id = $request->company_id;

      $plant->save();
      Session::flash('message', 'Planta guardado con éxito!');
      return redirect()->route('plants.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $plant = Plant::find($id);
        $plant->delete();
        return response()->json([
          'status' => 'ok'
      ]);
    }
}
