<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Company;
use View;
use Session;


class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $companies = Company::all();

        // load the view and pass the users
        return View::make('companies.index')
            ->with('companies', $companies);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255|unique:companys',
            'email' => 'required|email|max:255|unique:companys',
            'address' => 'required|max:255',
            'ciudad' => 'required|max:255',
            'phone' => 'required|numeric|max:20',
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        /*return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'ciudad' => $data['ciudad'],
            'phone' => $data['phone'],
        ]);*/

        return view('companies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/

        $company = new Company;
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->phone = $request->phone;

        $company->save();
        Session::flash('message', 'Empresa guardada con éxito!');
        return redirect()->route('companies.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $company = Company::find($id);
        return view('companies.edit')->with('company', $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /*$this->validate($request, array(
            'name' => 'required|max:255|unique:companies',
            'email' => 'required|email|max:255|unique:companies',
            'address' => 'max:255',
            'city' => 'max:255',
            'phone' => 'numeric|max:20',
        ));*/
        $company = Company::find($id);
        $company->name = $request->name;
        $company->email = $request->email;
        $company->address = $request->address;
        $company->city = $request->city;
        $company->phone = $request->phone;

      $company->save();
      Session::flash('message', 'Empresa guardado con éxito!');
      return redirect()->route('companies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $company = Company::find($id);
        $company->delete();
        return response()->json([
          'status' => 'ok'
      ]);
    }
}
