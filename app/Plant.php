<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Plant extends Model
{
	use Notifiable;

    public function plant()
    {
        return $this->belongsTo('App\Plant');
    }

    public function empresa()
	{
		return $this->belongsToMany('App\User');
	} 
}
