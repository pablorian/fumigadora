@extends('layouts.app')

@section('extra_js')
<script>
$( document ).ready(function() {
  $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
});
</script>
<script src="/js/user_index.js" type="text/javascript"></script>
@endsection

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Usuarios</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline"> Empresa
                <i class="fa fa-angle-down"></i>
            </button>

        </div>
    </div>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> Usuarios
    <small>Listado</small>
</h1>

<div class="portlet-body">
    <div class="table-toolbar">
        <div class="row">
            <div class="col-md-12">
                <div class="btn-group pull-right">
                    <a id="sample_editable_1_new" class="btn sbold green" href="{!! route('plants.create'); !!}"> Agregar
                        <i class="fa fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="main_table">
        <thead>
            <tr>
                <th> Planta </th>
                <th> Empresa</th>
                <th> Miembro desde </th>
                <th> Actions </th>
            </tr>
        </thead>
        <tbody>
          @foreach ($plants as $plant)
            <tr class="odd gradeX">

                <td> {{$plant->name}} </td>
                <td>
                   <!--aca va el nombre de la empresa-->
                </td>
                <td class="center"> {{$plant->created_at}} </td>
                <td>
                    <div class="btn-group">
                        <a class="btn btn-xs green" href="{!! route('plants.edit', $plant->id) !!}" > Editar
                            <i class="fa fa-edit"></i>
                        </a>
                        <button class="btn btn-xs red bt_delete" type="button" data-url="{!! route('plants.destroy', $plant->id) !!}" > Eliminar
                            <i class="fa fa-remove"></i>
                        </button>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>


<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
@endsection
