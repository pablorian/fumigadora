@extends('layouts.app')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="/">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{!! route('plants.index'); !!}">Usuarios</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Editar usuario</span>
        </li>
    </ul>
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <button type="button" class="btn green btn-sm btn-outline"> Empresa
                <i class="fa fa-angle-down"></i>
            </button>

        </div>
    </div>
</div>
<!-- END PAGE BAR -->

<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="caption font-red-sunglo">
          <i class="icon-settings font-red-sunglo"></i>
          <span class="caption-subject bold uppercase"> Nuevo Usuario</span>
      </div>
  </div>
  <div class="portlet-body form">
      {!! Form::model($plant, ['method' => 'PUT', 'route' => ['plants.update', $plant->id]]) !!}
          <div class="form-body">

            <div class="form-group">
              <label>Nombre</label>
              {{ Form::text('name', null, array('class' => 'form-control input-circle-right', 'placeholder' => 'Nombre')) }}
            </div>
            <div class="form-group">
              <label>Sumario</label>
              {{ Form::text('summary', null, array('class' => 'form-control input-circle-right', 'placeholder' => 'Sumario')) }}
            </div>
            <div class="form-group">
              <label>Observaciones</label>
              {{ Form::text('observations', null, array('class' => 'form-control input-circle-right', 'placeholder' => 'Observaciones')) }}
            </div>
            <div class="form-group">
              <label>Empresa</label>
              {{ Form::number('company_id', null, array('class' => 'form-control input-circle-right', 'placeholder' => 'Empresa')) }}
            </div>

          </div>
          <div class="form-actions">
              <button type="submit" class="btn blue">Guardar</button>
              <a href="{!! route('plants.index') !!}" class="btn default">Cancelar</a>
          </div>
      {!! Form::close() !!}
  </div>
</div>
@endsection

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
