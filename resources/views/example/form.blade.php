@extends('layouts.app')

@section('content')
<div class="portlet light bordered">
  <div class="portlet-title">
      <div class="caption font-red-sunglo">
          <i class="icon-settings font-red-sunglo"></i>
          <span class="caption-subject bold uppercase"> Default Form</span>
      </div>
  </div>
  <div class="portlet-body form">
      <form role="form">
          <div class="form-body">
              <div class="form-group">
                  <label>Email Address</label>
                  <div class="input-group">
                      <span class="input-group-addon">
                          <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" class="form-control" placeholder="Email Address"> </div>
              </div>
              <div class="form-group">
                  <label>Circle Input</label>
                  <div class="input-group">
                      <span class="input-group-addon input-circle-left">
                          <i class="fa fa-envelope"></i>
                      </span>
                      <input type="text" class="form-control input-circle-right" placeholder="Email Address"> </div>
              </div>
              <div class="form-group">
                  <label for="exampleInputPassword1">Password</label>
                  <div class="input-group">
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                      <span class="input-group-addon">
                          <i class="fa fa-user font-red"></i>
                      </span>
                  </div>
              </div>
              <div class="form-group">
                  <label>Left Icon</label>
                  <div class="input-icon">
                      <i class="fa fa-bell-o font-green"></i>
                      <input type="text" class="form-control" placeholder="Left icon"> </div>
              </div>
              <div class="form-group">
                  <label>Left Icon(.input-sm)</label>
                  <div class="input-icon input-icon-sm">
                      <i class="fa fa-bell-o"></i>
                      <input type="text" class="form-control input-sm" placeholder="Left icon"> </div>
              </div>
              <div class="form-group">
                  <label>Left Icon(.input-lg)</label>
                  <div class="input-icon input-icon-lg">
                      <i class="fa fa-bell-o"></i>
                      <input type="text" class="form-control input-lg" placeholder="Left icon"> </div>
              </div>
              <div class="form-group">
                  <label>Right Icon</label>
                  <div class="input-icon right">
                      <i class="fa fa-microphone fa-spin font-blue"></i>
                      <input type="text" class="form-control" placeholder="Right icon"> </div>
              </div>
              <div class="form-group">
                  <label>Right Icon(.input-sm)</label>
                  <div class="input-icon input-icon-sm right">
                      <i class="fa fa-bell-o"></i>
                      <input type="text" class="form-control input-sm" placeholder="Left icon"> </div>
              </div>
              <div class="form-group">
                  <label>Right Icon(.input-lg)</label>
                  <div class="input-icon input-icon-lg right">
                      <i class="fa fa-bell-o font-green"></i>
                      <input type="text" class="form-control input-lg" placeholder="Left icon"> </div>
              </div>
              <div class="form-group">
                  <label>Circle Input</label>
                  <div class="input-icon right">
                      <i class="fa fa-microphone"></i>
                      <input type="text" class="form-control input-circle" placeholder="Right icon"> </div>
              </div>
              <div class="form-group">
                  <label>Input with Icon</label>
                  <div class="input-group input-icon right">
                      <span class="input-group-addon">
                          <i class="fa fa-envelope font-purple"></i>
                      </span>
                      <i class="fa fa-exclamation tooltips" data-original-title="Invalid email." data-container="body"></i>
                      <input id="email" class="input-error form-control" type="text" value=""> </div>
              </div>
              <div class="form-group">
                  <label>Input With Spinner</label>
                  <input class="form-control spinner" type="text" placeholder="Process something"> </div>
              <div class="form-group">
                  <label>Static Control</label>
                  <p class="form-control-static"> email@example.com </p>
              </div>
              <div class="form-group">
                  <label>Disabled</label>
                  <input type="text" class="form-control" placeholder="Disabled" disabled=""> </div>
              <div class="form-group">
                  <label>Readonly</label>
                  <input type="text" class="form-control" placeholder="Readonly" readonly=""> </div>
              <div class="form-group">
                  <label>Dropdown</label>
                  <select class="form-control">
                      <option>Option 1</option>
                      <option>Option 2</option>
                      <option>Option 3</option>
                      <option>Option 4</option>
                      <option>Option 5</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Multiple Select</label>
                  <select multiple="" class="form-control">
                      <option>Option 1</option>
                      <option>Option 2</option>
                      <option>Option 3</option>
                      <option>Option 4</option>
                      <option>Option 5</option>
                  </select>
              </div>
              <div class="form-group">
                  <label>Textarea</label>
                  <textarea class="form-control" rows="3"></textarea>
              </div>
              <div class="form-group">
                  <label for="exampleInputFile1">File input</label>
                  <input type="file" id="exampleInputFile1">
                  <p class="help-block"> some help text here. </p>
              </div>
              <div class="form-group">
                  <label>Checkboxes</label>
                  <div class="mt-checkbox-list">
                      <label class="mt-checkbox"> Checkbox 1
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-checkbox"> Checkbox 2
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-checkbox"> Checkbox 3
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                  </div>
              </div>
              <div class="form-group">
                  <label>Outline Checkboxes</label>
                  <div class="mt-checkbox-list">
                      <label class="mt-checkbox mt-checkbox-outline"> Checkbox 1
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-checkbox mt-checkbox-outline"> Checkbox 2
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-checkbox mt-checkbox-outline"> Checkbox 3
                          <input type="checkbox" value="1" name="test">
                          <span></span>
                      </label>
                  </div>
              </div>
              <div class="form-group">
                  <label>Inline Checkboxes</label>
                  <div class="mt-checkbox-inline">
                      <label class="mt-checkbox">
                          <input type="checkbox" id="inlineCheckbox1" value="option1"> Checkbox 1
                          <span></span>
                      </label>
                      <label class="mt-checkbox">
                          <input type="checkbox" id="inlineCheckbox2" value="option2"> Checkbox 2
                          <span></span>
                      </label>
                      <label class="mt-checkbox mt-checkbox-disabled">
                          <input type="checkbox" id="inlineCheckbox3" value="option3" disabled=""> Disabled
                          <span></span>
                      </label>
                  </div>
              </div>
              <div class="form-group">
                  <label>Radios</label>
                  <div class="mt-radio-list">
                      <label class="mt-radio"> Radio 1
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-radio"> Radio 2
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-radio"> Radio 3
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                  </div>
              </div>
              <div class="form-group">
                  <label>Outline Radios</label>
                  <div class="mt-radio-list">
                      <label class="mt-radio mt-radio-outline"> Radio 1
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-radio mt-radio-outline"> Radio 2
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                      <label class="mt-radio mt-radio-outline"> Radio 3
                          <input type="radio" value="1" name="test">
                          <span></span>
                      </label>
                  </div>
              </div>
              <div class="form-group">
                  <label>Inline Radio</label>
                  <div class="mt-radio-inline">
                      <label class="mt-radio">
                          <input type="radio" name="optionsRadios" id="optionsRadios4" value="option1" checked=""> Option 1
                          <span></span>
                      </label>
                      <label class="mt-radio">
                          <input type="radio" name="optionsRadios" id="optionsRadios5" value="option2"> Option 2
                          <span></span>
                      </label>
                      <label class="mt-radio">
                          <input type="radio" name="optionsRadios" id="optionsRadios6" value="option3" disabled=""> Disabled
                          <span></span>
                      </label>
                  </div>
              </div>
          </div>
          <div class="form-actions">
              <button type="submit" class="btn blue">Submit</button>
              <button type="button" class="btn default">Cancel</button>
          </div>
      </form>
  </div>
</div>
@endsection

@section('content2')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
