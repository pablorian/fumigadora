<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::group(['middleware' => ['web']], function () {
  Route::get('/', function () {
      return view('welcome');
  });

  Route::get('/example_form', function () {
      return view('example.form');
  });
	
	Auth::routes();

	Route::get('/home', 'HomeController@index');
	Route::resource('users', 'UserController');
	Route::resource('companies', 'CompanyController');
  Route::resource('plants', 'PlantController');
  Route::resource('technicians', 'TechnicianController');
});