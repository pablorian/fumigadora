<?php

class CompanyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->delete();
        $companies = factory('App\Company')->create(10);
        DB::table('companies')->insert($companies);
    }
}
