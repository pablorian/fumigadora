<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plants', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->text('summary')->nullable();
            $table->text('observations')->nullable();
            $table->string('image_plane')->nullable();
            $table->integer('company_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('plants', function(Blueprint $table) {
          $table->foreign('company_id')->references('id')->on('companies')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plants', function(Blueprint $table) {
            $table->dropForeign('plants_company_id_foreign');
        });
        Schema::dropIfExists('plants');
    }
}
