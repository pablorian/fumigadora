<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCorrectiveActionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corrective_actions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('responsible_id')->unsigned();
            $table->timestamps();
        });

        Schema::table('corrective_actions', function(Blueprint $table) {
          $table->foreign('responsible_id')->references('id')->on('responsibles')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('corrective_actions', function(Blueprint $table) {
            $table->dropForeign('corrective_actions_responsible_id_foreign');
        });
        Schema::dropIfExists('corrective_actions');
    }
}
